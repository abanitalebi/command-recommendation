
import os
import _pickle as cPickle

import matplotlib.mlab as mlab
from matplotlib import pyplot as plt
import numpy as np
import tqdm

from constants import CODE_BASE_DIRPATH
from utils.common_utils import convert_list_of_lists_to_1d_numpy_array

idle_time_threshold = 100    # Minimum time in second, between every two session/sentence of commands/words


class Command(object):
    def __init__(self, command, timestamp):
        self.command = command
        self.timestamp = timestamp    # time that command was executed.
        """
        Note that the difference of two timestamps is in units of seconds.
        If required, to extract different information from timestamp can use commands such as the ones below	
        month= int(datetime.datetime.fromtimestamp(int (row[6] )/1000).strftime('%m'))
        year= int(datetime.datetime.fromtimestamp(int (row[6] )/1000).strftime('%y')) """


class User(object):
    def __init__(self, userID, commands_list):
        self.userID=userID
        self.commands_list = commands_list 		# each entry of  commands_list is a Command object


if __name__ == '__main__':

    # input file
    users_data = cPickle.load(open("users_4307_dict", "rb"))
    # users_data = cPickle.load(open("user_0_dict", "rb"))

    # output text file
    command_dataset_filepath = os.path.join(CODE_BASE_DIRPATH, "data/input.txt")

    # user_0_data = {'100558': users_data['100558']}
    # del users_data
    # cPickle.dump(user_0_data, open("user_0_dict", "wb"))

    plot_timestamp_histogram = False

    user_timestamps = []
    user_commands = []
    txt_file = open(command_dataset_filepath, "w+")
    for user_number, user_data in tqdm.tqdm(users_data.items()):
        user_id = user_data.userID
        user_commands_data = user_data.commands_list

        this_session_times = []
        this_session_commands = []
        previous_time = -1
        for idx, command in enumerate(user_commands_data):
            this_time = float(user_commands_data[idx].timestamp) / 1000.
            this_command = str(user_commands_data[idx].command)
            # print("UserID: {}, Timestamp: {}, Commands {}".format(user_id, this_time, this_command))

            # Break commands into sessions
            if this_time <= previous_time + idle_time_threshold:
                this_session_times.append(this_time)
                this_session_commands.append(this_command)
            elif (len(this_session_times) >= 2) and (len(this_session_commands) >= 2):
                # Add an example
                if plot_timestamp_histogram:
                    user_timestamps.append(this_session_times)
                    user_commands.append(this_session_commands)
                # print(len(this_session_times))

                # write to a text file
                for one_command in this_session_commands:
                    txt_file.write(one_command + " ")

                # reset for the next example
                this_session_times = []
                this_session_commands = []

            previous_time = this_time

        txt_file.write("\n")

        # Plot a histogram
        if plot_timestamp_histogram:
            all_user_timestamps = convert_list_of_lists_to_1d_numpy_array(user_timestamps)
            n, bins, patches = plt.hist(all_user_timestamps, normed=True, bins=300)
            plt.xlabel('Time Stamp')
            plt.ylabel('Probability')
            plt.title("User ID: {}".format(user_id))
            # plt.show()
            plt.savefig("sample_timestamps_histogram.png")


