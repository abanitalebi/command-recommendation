# numpy_utils.py
# Utilities for working with numpy
import itertools
import numpy as np


def convert_list_of_lists_to_1d_numpy_array(list_of_lists):
    return np.array(list(itertools.chain(*list_of_lists)))
