
# Utility to split a text file data to train/test/validation segments

import os

from constants import CODE_BASE_DIRPATH

import numpy as np


def split_text_file(text_filepath, train=0.8, validation=0.02, test=0.18):
    with open(text_filepath, 'rb') as txt_file:
        data = txt_file.readlines()

    num_lines = len(data)
    perm = np.random.permutation(num_lines)

    train_lines = data[0:int(len(perm) * train)]
    validation_lines = data[int(len(perm) * train) : int(len(perm) * (train+validation))]
    test_lines = data[int(len(perm) * (train+validation)) : ]

    with open(os.path.join(CODE_BASE_DIRPATH, 'data/train/input.txt'), 'wb') as txt_file:
        txt_file.writelines(train_lines)

    with open(os.path.join(CODE_BASE_DIRPATH, 'data/validation/input.txt'), 'wb') as txt_file:
        txt_file.writelines(validation_lines)

    with open(os.path.join(CODE_BASE_DIRPATH, 'data/test/input.txt'), 'wb') as txt_file:
        txt_file.writelines(test_lines)

    return


if __name__ == "__main__":

    text_data_filepath = os.path.join(CODE_BASE_DIRPATH, 'data/input.txt')
    split_text_file(text_data_filepath)