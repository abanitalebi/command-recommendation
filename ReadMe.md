# Command Generation Using Deep Long Short Term Memory Networks

## Introduction
Despite an abundance of commands to make tasks easier to perform, 
the users of feature-rich applications, such as development environments 
and AutoCAD applications, use only a fraction of the commands available 
due to a lack of awareness of the existence of many commands. 
Earlier work has shown that command recommendation can improve the usage 
of a range of commands available within such applications. 

This repo aims to learn a computational model of command recommendation, 
using deep Long Short Term Memory (LSTM) networks. 

## Requirements
Tested on Python 3.5 with TensorFlow 1.8

## Citation
This code is available for research use under MIT license. 
Please cite the following papers if this code used for a publication:

[1] S. Zolaktaf, "What to learn next: recommending commands in a 
feature-rich environment," Master's Thesis, University of British Columbia, 2015.

[2] S. Zolaktaf, "What to Learn Next: Recommending Commands in a 
Feature-Rich Environment," 2015 IEEE 14th International Conference on 
Machine Learning and Applications (ICMLA), Miami, FL, USA, Dec. 2015.