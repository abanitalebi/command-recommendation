from __future__ import print_function
import tensorflow as tf

import argparse
import os

import tqdm
from six.moves import cPickle

import numpy as np
from matplotlib import pyplot as plt

from constants import CODE_BASE_DIRPATH
from utils.model import Model


def generate_examples(num_command_sessions, num_commands, prime_command, sample, pick, width, quiet,
                      num_predictions_per_word, model, sess, words, vocab):

    print("    " + prime_command)
    for _ in range(num_command_sessions):
        generated_example = model.sample(sess, words, vocab, num_commands, prime_command, sample, pick, width,
                                         quiet, num_predictions_per_word)
        print("    " + generated_example)

    print("")

    return generated_example


def run_validation(save_dir, max_num_predictions_per_word, command_dataset_filepath, num_command_sessions,
                   num_commands, sample, pick, width, quiet, save_plot=True, new_graph=False):

    ############################################
    with open(os.path.join(save_dir, 'config.pkl'), 'rb') as f:
        saved_args = cPickle.load(f)
    with open(os.path.join(save_dir, 'words_vocab.pkl'), 'rb') as f:
        words, vocab = cPickle.load(f)

    if new_graph:
        graph = tf.Graph()
    else:
        graph = tf.get_default_graph()

    with graph.as_default() as g:
        sess = tf.Session(graph=g)
        model = Model(saved_args, True)
        tf.global_variables_initializer().run(session=sess)
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
        ############################################


        ############################################
        total_examples = 0
        correct_predictions = np.zeros(max_num_predictions_per_word)
        with open(command_dataset_filepath, 'rb') as txt_file:
            for counter, line in enumerate(txt_file):

                print('    User: {}'.format(counter))
                user_commands_str = line
                if len(user_commands_str) > 0:
                    user_commands_bytes = user_commands_str.split()
                    user_commands = [i.decode('utf-8') for i in user_commands_bytes]
                    unique_commands = np.unique(user_commands)
                    last_command = None
                    for command in unique_commands[::-1]:
                        if np.sum(unique_commands == command) == 1:
                            last_command = command
                            break
                    if last_command is not None:
                        last_command_idx = user_commands.index(last_command)
                        # prime_words = user_commands[max(0, last_command_idx - 10):last_command_idx]
                        prime_words = user_commands[:last_command_idx]
                        prime_command = ' '.join(prime_words)
                        predictions = generate_examples(num_command_sessions, num_commands, prime_command, sample, pick,
                                                        width, quiet, max_num_predictions_per_word, model, sess, words, vocab)

                        # calculate the number of correct predictions for i =0:max_predictions_per_word
                        predicted_commands = predictions.split()[-max_num_predictions_per_word:]
                        for idx in range(max_num_predictions_per_word):
                            if idx == 0:
                                if last_command == predicted_commands[0]:
                                    correct_predictions[idx] += 1
                            elif last_command in predicted_commands[:idx+1]:
                                correct_predictions[idx] += 1
                        total_examples += 1

        sess.close()

    acc = np.array(correct_predictions) / total_examples
    print('Accuracy: {}'.format(acc))

    if save_plot:
        fig = plt.figure()
        plt.plot(list(range(max_num_predictions_per_word)), acc * 100.0)
        plt.ylabel('Recall (%)')
        plt.xlabel('Number of recommended commands')
        fig.savefig(os.path.join(CODE_BASE_DIRPATH, 'performance_evaluations.png'))

    return acc


if __name__ == '__main__':

    os.environ['CUDA_VISIBLE_DEVICES'] = '0'

    command_dataset_filepath = os.path.join(CODE_BASE_DIRPATH, 'data/test/input.txt')
    save_dir = os.path.join(CODE_BASE_DIRPATH, 'save')
    num_command_sessions = 1
    num_commands = 1
    max_num_predictions_per_word = 20
    prime_command = ''
    sample = 1
    pick = 1
    width = 4
    quiet = True

    acc = run_validation(save_dir, max_num_predictions_per_word, command_dataset_filepath, num_command_sessions,
                         num_commands, sample, pick, width, quiet, save_plot=True, new_graph=False)