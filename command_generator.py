from __future__ import print_function
import tensorflow as tf

import argparse
import os
from six.moves import cPickle

from constants import CODE_BASE_DIRPATH
from utils.model import Model


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--save_dir', type=str, default=os.path.join(CODE_BASE_DIRPATH, 'save'),
                        help='model directory to load stored checkpointed models from')
    parser.add_argument('-num_commands', type=int, default=50,
                        help='number of words to sample')
    parser.add_argument('--prime_command', type=str, default=' ',
                        help='prime text')
    parser.add_argument('--pick', type=int, default=1,
                        help='1 = weighted pick, 2 = beam search pick')
    parser.add_argument('--width', type=int, default=4,
                        help='width of the beam search')
    parser.add_argument('--sample', type=int, default=1,
                        help='0 to use max at each timestep, 1 to sample at each timestep, 2 to sample on spaces')
    parser.add_argument('--num_command_sessions', '-c', type=int, default=1,
                        help='number of samples to print')
    parser.add_argument('--quiet', '-q', default=False, action='store_true',
                        help='suppress printing the prime text (default false)')

    args = parser.parse_args()
    generate_examples(args.save_dir, args.num_command_sessions, args.num_commands, args.prime_command, args.sample,
                      args.pick, args.width, args.quiet)


def generate_examples(save_dir, num_command_sessions, num_commands, prime_command, sample, pick, width, quiet):
    with open(os.path.join(save_dir, 'config.pkl'), 'rb') as f:
        saved_args = cPickle.load(f)
    with open(os.path.join(save_dir, 'words_vocab.pkl'), 'rb') as f:
        words, vocab = cPickle.load(f)
    model = Model(saved_args, True)
    with tf.Session() as sess:
        tf.global_variables_initializer().run()
        saver = tf.train.Saver(tf.global_variables())
        ckpt = tf.train.get_checkpoint_state(save_dir)
        if ckpt and ckpt.model_checkpoint_path:
            saver.restore(sess, ckpt.model_checkpoint_path)
            for _ in range(num_command_sessions):
                generated_example = model.sample(sess, words, vocab, num_commands, prime_command, sample, pick, width, quiet)
                print(generated_example)


if __name__ == '__main__':

    # main()

    save_dir = os.path.join(CODE_BASE_DIRPATH, 'save')
    num_command_sessions = 1
    num_commands = 50
    prime_command = ''
    sample = 1
    pick = 1
    width = 4
    quiet = False

    generate_examples(save_dir, num_command_sessions, num_commands, prime_command, sample, pick, width, quiet)
